
===================================



Original credit file from the Total Realism Mod for Darkest Hour which is merely humbly tweaked here. Thank you for your hard work everyone below.
====================================

#8§WTotal Realism Project
#8§Wa modification of Darkest Hour


#6§WProject Leader

#5§YKane


#6§WLEAD AI PROGRAMMER

#5§YDerStudti


#6§WRESEARCH/EVENTS

#5§YLord Rommel
#5§YDerStudti
#5§YKane
#5§YAser
#5§YStahl-Opa
#5§YBahzresh

#6§WGAME MECHANICS

#5§YDerStudti
#5§YKane
#5§YMars


#6§WGRAPHICS

#5§YDendro
#5§YLord Rommel
#5§YDerStudti
#5§YAser
#5§Ybestmajor

#5§Finnish pics by SA-kuva (Finnish Armed Forces photograph)

#6§WCOLOURISING

#5§YKönigstiger
#5§YSeeme
#5§Ybeagletank


#6§WSPRITES GENEROUSLY SUPPLIED BY
#5§YDR.RARE


#6§WCOPYRIGHT FOR MOST OF THE COLORIZED NAVAL PHOTOS
#5§Yirootoko_jr


#6§WTECH TREE

#5§YDerStudti
#5§YKane
#5§YLord Rommel


#6§WBETA TESTERS (in alphabetical order)

#5§YAdmiral RV
#5§YArdrianer
#5§YAser
#5§YCounter
#5§YDerStudti
#5§YJPP
#5§Yjeannen
#5§YKarste70
#5§YLeonaru
#5§YLinde
#5§YLord Rommel
#5§YMars
#5§YMajor
#5§YOorlog
#5§YOberst Klink
#5§Yrwglaub
#5§YStahl-Opa
#5§Ytomaszpudlo
#5§YWilhelm Klink
#5§YZergel


#6§WMP BETA GROUP (in alphabetical order)

#5§YBahzresh
#5§YDerStudti
#5§YKane
#5§YLinde
#5§YLord Rommel
#5§Ymann85xxx
#5§YMars
#5§YNoHopeNoFear


#6§WMP FORMER BETA GROUP (in alphabetical order)

#5§YCounter
#5§YGina
#5§YGurkbert
#5§YJPP
#5§Yjeannen
#5§YKarste70
#5§YMajor
#5§Ywaynetraub3
#5§YZeitenwende


#6§Special Thanks
#5§Solfall for his Norwegian TT-Mod
#5§eatmeat for his Balkan information
#5§Shaun - unser Schaf!

#8§WTotal Realism Project, the original team:


#6§WProject Leader

#4§YLothos


#6§WLEAD PROGRAMMER

#4§YHendriks


#6§WMAJOR CONTRIBUTORS

#4§YLuxor
#4§YCCM von Hausser


#6§WRESEARCH/EVENTS

#4§YAllenBy
#4§YSamu
#4§YPapero
#4§YWhiteJP
#4§YKane
#4§YBulgarian
#4§YArmoured Warrior
#4§YPerkele


#6§WMAP CHANGES

#4§YMarlok


#6§WGRAPHICS

#4§YXieChengnuo
#4§YBonzosarmy
#4§YFuror Teutonicus
#4§Y|AXiN|
#4§YKippler
#4§Neil "Jed" Jedrzejewski (3D Consultant)


#6§WTECH TREE

#4§YPB-DK
#4§YCCM von Hausser
#4§YLuxor
#4§YKane


#6§WMUSICS

#4§YOtto_Kampf


#6§WBETA TESTERS

#4§YA N Droid
#4§YAndrew EAGLE
#4§YAtahualpa
#4§YBabe
#4§YBrasidas
#4§YColonel Warden
#4§YdaMs
#4§Ydanny
#4§YDerekP
#4§YFalstaff
#4§YHegemonist
#4§Yhelio1989
#4§Yhendriks
#4§Yhkbhsi
#4§Yjmschaub
#4§YKyper
#4§Yladybug
#4§Ylancealot
#4§YLordban
#4§YItm6942
#4§YMrCrush
#4§YNiKuTa
#4§YNnexus
#4§YNordehim
#4§Yog0172
#4§YOorlog
#4§YPaxMondo
#4§Yquaz
#4§Yranger mike
#4§YRevan1985
#4§YRhetoric
#4§YRorschach
#4§Yrwglaub
#4§Ysemperfi
#4§Ysfleafs
#4§YSiRanger
#4§YVolmar
#4§YZwiback


#6§WOTHER CONTRIBUTORS

#4§YArtic Fox
#4§YPhil K
#4§YKevin McCarthy
#4§JanMasterson
#4§KingMississippi
#4§Oorlog
#4§llib
#4§Walsingham
#4§YMcNaughton
#4§YCORE 0.64 Team
#4§YGraphic Improvement Team (SR)

-END